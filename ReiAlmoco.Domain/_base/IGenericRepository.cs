﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReiAlmoco.Domain._base
{
    public interface IGenericRepository<T>
    {
        T GetById(Guid id);
        List<T> List();
        void Add(T entity);
        void Update(T entity);
        bool Delete(Guid id);
    }
}
