﻿using Microsoft.EntityFrameworkCore;
using ReiAlmoco.Data._base;
using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain._utils;
using ReiAlmoco.Domain.Interfaces.Repositories;
using ReiAlmoco.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiAlmoco.Data.Repositories
{
    public class PretendenteRepository : GenericRepository<Pretendente>, IPretendenteRepository
    {
        private readonly AppDbContext _context;

        public PretendenteRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public Pretendente GetByEmail(string email)
        {
            return _context.Set<Pretendente>().FirstOrDefault(p => p.Email.Equals(email));

        }

        public Pretendente  GetVotingByWeek(DateTime date, bool isDescending)
        {
            var sundayDate = Functions.GetFirstDateOfWeek(date);
            var saturdayDate = Functions.GetLastDateOfWeek(date);

            var pretendentes = _context.Set<Pretendente>()
                .Where(p=> p.Votos.All(v=> 
                    v.Data>= sundayDate && v.Data <= saturdayDate
                    )).AsQueryable();



            if(isDescending)
                return pretendentes.OrderByDescending(p => p.Votos.Count())
                   .Include(p=> p.Votos).FirstOrDefault();
            else
                return pretendentes.OrderBy(p => p.Votos.Count())
                    .Include(p => p.Votos).FirstOrDefault();

        }

        public Pretendente GetByMostVotedByDay(DateTime data)
        {
            return _context.Set<Pretendente>()
                .Where(p => p.Votos.All(v => v.Data.Date.Equals(data.Date)))
                .OrderByDescending(p => p.Votos.Count())
                .Include(p=> p.Votos).FirstOrDefault();



        }



    }
}
