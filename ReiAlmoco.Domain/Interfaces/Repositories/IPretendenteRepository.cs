﻿using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain.Models;
using System;
using System.Collections.Generic;

namespace ReiAlmoco.Domain.Interfaces.Repositories
{
    public interface IPretendenteRepository : IGenericRepository<Pretendente>
    {
        Pretendente GetVotingByWeek(DateTime date, bool isDescending);
        Pretendente GetByMostVotedByDay(DateTime data);

        Pretendente GetByEmail(string email);

    }
}
