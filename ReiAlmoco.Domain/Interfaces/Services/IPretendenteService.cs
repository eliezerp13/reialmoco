﻿using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain._utils;
using ReiAlmoco.Domain.Dtos;
using ReiAlmoco.Domain.Models;
using ReiAlmoco.Domain.ViewModels;
using System;

namespace ReiAlmoco.Domain.Interfaces.Services
{

    public interface IPretendenteService
    {
        HttpServiceResponse<Pretendente> Add(PretendenteDto pretendenteDto);
        HttpServiceResponse<Pretendente> AddVoto(VotoDto votoDto);
        HttpServiceResponse<PretendentesScoreViewModel> GetMoreFewerVoted(PretendenteFilterType pretendenteFilterType, DateTime date);

    }
}
