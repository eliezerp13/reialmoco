﻿using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReiAlmoco.Domain.Interfaces.Repositories
{
    public interface IParametroRepository : IGenericRepository<Parametro>
    {
        List<Parametro> ListByChaves(List<string> chaves);

    }
}
