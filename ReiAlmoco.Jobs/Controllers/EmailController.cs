﻿using Microsoft.AspNetCore.Mvc;
using ReiAlmoco.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReiAlmoco.Jobs.Controllers
{
    [Route("api/[Controller]")]
    public class EmailController : Controller
    {
        private readonly IEmailService _emailService;

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var response = _emailService.DispatchEmailToKingOfTheDay();
                return new ObjectResult(response.Message);

            }
            catch (Exception e)
            {
                return StatusCode(500, new { Message = e.Message });
            }

        }
    }
}
