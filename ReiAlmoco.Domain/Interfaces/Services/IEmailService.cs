﻿using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReiAlmoco.Domain.Interfaces.Services
{
    public interface IEmailService
    {
        HttpServiceResponse<PretendentesScoreViewModel> DispatchEmailToKingOfTheDay();
    }
}
