﻿using Microsoft.AspNetCore.Mvc;
using ReiAlmoco.Domain._utils;
using ReiAlmoco.Domain.Dtos;
using ReiAlmoco.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ReiAlmoco.API.Controllers
{
    namespace ReiAlmoco.API.Controllers
    {
        [Route("api/[Controller]")]
        [ApiController]
        public class PretendenteController : ControllerBase
        {
            private readonly IPretendenteService _pretendenteService;

            public PretendenteController(IPretendenteService pretendenteService)
            {
                _pretendenteService = pretendenteService;
            }

            [HttpPost]
            public IActionResult Add([FromBody]PretendenteDto pretendenteDto)
            {
                try
                {
                    var response = _pretendenteService.Add(pretendenteDto);
                    return StatusCode((int)response.StatusCode,new { message = response.Message });
                }
                catch (Exception e)
                {
                    return StatusCode(500, new { Message = e.Message });
                }

            }

            [HttpPost]
            [Route("AddVoto")]
            public IActionResult AddVoto([FromBody]VotoDto votoDto)
            {
                try
                {
                    var response = _pretendenteService.AddVoto(votoDto);

                    return StatusCode((int)response.StatusCode,new { message = response.Message });
                }
                catch (Exception e)
                {
                    return StatusCode(500, new { Message = e.Message });
                }

            }


            [HttpGet]
            public IActionResult ListByMoreorFewerVoted([FromQuery] PretendenteFilterType pretendenteFilterType, [FromQuery] DateTime date)
            {
                try
                {
                    var response = _pretendenteService.GetMoreFewerVoted(pretendenteFilterType, date);
                    if(response.StatusCode != HttpStatusCode.OK)
                        return StatusCode((int)response.StatusCode, new { message = response.Message });

                    return new ObjectResult(response.Object);
                }
                catch (Exception e)
                {
                    return StatusCode(500, new { Message = e.Message });
                }

            }
        }
    }
}
