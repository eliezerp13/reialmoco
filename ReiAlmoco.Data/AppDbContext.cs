﻿using Microsoft.EntityFrameworkCore;
using ReiAlmoco.Domain.Models;
using System.Threading.Tasks;

namespace ReiAlmoco.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        #region DbSets
            public DbSet<Pretendente> Pretendentes { get; set; }
            public DbSet<Voto> Votos { get; set; }
            public DbSet<Parametro> Parametros { get; set; }


        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //FK_VOTOS_PRETENDENTES
            modelBuilder.Entity<Pretendente>()
                .HasMany(p => p.Votos)
                .WithOne(v => v.Pretendente);
        }
    }
}
