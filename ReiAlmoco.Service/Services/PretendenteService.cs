﻿using ReiAlmoco.Domain;
using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain._utils;
using ReiAlmoco.Domain.Dtos;
using ReiAlmoco.Domain.Interfaces.Repositories;
using ReiAlmoco.Domain.Interfaces.Services;
using ReiAlmoco.Domain.Models;
using ReiAlmoco.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace ReiAlmoco.Service.Services
{
    public class PretendenteService : IPretendenteService
    {
        public readonly IPretendenteRepository _repository;
        public readonly IParametroRepository _parametroRepository;
        public PretendenteService(IPretendenteRepository repository, IParametroRepository parametroRepository)
        {
            _repository = repository;
            _parametroRepository = parametroRepository;
        }
        public HttpServiceResponse<Pretendente> Add(PretendenteDto pretendenteDto)
        {

            var pretendente = _repository.GetByEmail(pretendenteDto.Email);

            if (pretendente != null)
                return new HttpServiceResponse<Pretendente>
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Message  =  Recourses.ExistingEmail
                };

            pretendente = new Pretendente(pretendenteDto.Nome, pretendenteDto.Email, pretendenteDto.Foto);

            _repository.Add(pretendente);
            return new HttpServiceResponse<Pretendente>
            {
                StatusCode = HttpStatusCode.OK,
                Message = Recourses.PretendenteSuccess
               
            };
        }

        public HttpServiceResponse<Pretendente> AddVoto(VotoDto votoDto)
        {

            var listParametro = new List<string>();
            listParametro.Add(consts.InitialVotingKeyName);
            listParametro.Add(consts.EndVotingKeyName);
            var parametros = _parametroRepository.ListByChaves(listParametro);
            
            var initialVotingHourParametro = parametros.Where(
                p => p.Chave.Equals(consts.InitialVotingKeyName)).FirstOrDefault();

            var endVotingHourParametro = parametros.Where(
                p => p.Chave.Equals(consts.EndVotingKeyName)).FirstOrDefault();

            if (!Functions.isVotingHour(int.Parse(initialVotingHourParametro.Valor), int.Parse(endVotingHourParametro.Valor)))
                return new HttpServiceResponse<Pretendente>
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = Recourses.InvalidVotingHour
                };

            var pretendente = _repository.GetById(votoDto.IdUsuarioVotado);
             if (pretendente == null)
                return new HttpServiceResponse<Pretendente>
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = Recourses.NoExistentPretendente 

                };

            var voto = new Voto(votoDto.IdUsuarioVotado);
            pretendente.Votos.Add(voto);

            _repository.Update(pretendente);
            return new HttpServiceResponse<Pretendente>
            {
                StatusCode = HttpStatusCode.OK,
                Message = Recourses.VotoSuccess 
            };
        }

        public HttpServiceResponse<PretendentesScoreViewModel> GetMoreFewerVoted(PretendenteFilterType pretendenteFilterType, DateTime date)
        {
            Pretendente pretendente;
            switch (pretendenteFilterType)
            {
                case PretendenteFilterType.MoreVotedByDay:
                    pretendente = _repository.GetByMostVotedByDay(date);
                    break;

                case PretendenteFilterType.MoreVotedByWeek:
                    pretendente = _repository.GetVotingByWeek(date, true);
                    break;

                case PretendenteFilterType.FewerVotedByWeek:
                    pretendente = _repository.GetVotingByWeek(date, false);
                    break;
                default:
                    return new HttpServiceResponse<PretendentesScoreViewModel>
                    {
                        StatusCode = HttpStatusCode.NotFound,
                        Message = Recourses.NotVotings
                    };
            }
            PretendentesScoreViewModel viewModel;
            

            if (pretendente == null)
                return new HttpServiceResponse<PretendentesScoreViewModel>
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Message = Recourses.NoExistentPretendenteByFilter
                };

            int totalVotos;
                if (pretendente.Votos == null)
                    totalVotos = 0;
                else totalVotos = pretendente.Votos.Count();

                viewModel = new PretendentesScoreViewModel
                {
                    Id = pretendente.Id,
                    Nome = pretendente.Nome,
                    Foto = pretendente.Foto,
                    Email = pretendente.Email,
                    TotalVotos = totalVotos
                };

             
            return new HttpServiceResponse<PretendentesScoreViewModel>
            {
                StatusCode = HttpStatusCode.OK,
                Object = viewModel
            };
        }
    }
}
