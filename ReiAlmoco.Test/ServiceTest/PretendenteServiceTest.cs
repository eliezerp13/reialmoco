﻿using Bogus;
using Moq;
using ReiAlmoco.Domain;
using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain._utils;
using ReiAlmoco.Domain.Dtos;
using ReiAlmoco.Domain.Interfaces.Repositories;
using ReiAlmoco.Domain.Interfaces.Services;
using ReiAlmoco.Domain.Models;
using ReiAlmoco.Service.Services;
using ReiAlmoco.Test._builders;
using System;
using System.Collections.Generic;
using System.Net;
using Xunit;

namespace ReiAlmoco.Test.ServiceTest
{
    public class PretendenteServiceTest
    {
        private readonly PretendenteDto _pretendenteDto;
        private readonly VotoDto _votoDto;
        private readonly List<string> _listChavesParametrosFilter;
        private readonly List<Parametro> _listParametrosVotingHour;
        private readonly Mock<IPretendenteRepository> _pretendenteRepositoryMock;
        private readonly Mock<IParametroRepository> _parametroRepositoryMock;
        private readonly PretendenteService _pretendenteService;
        private HttpServiceResponse<Pretendente> _response;
        public PretendenteServiceTest()
        {
            var faker = new Faker();
            _pretendenteDto = new PretendenteDto
            {
                Nome = faker.Person.FullName,
                Email = faker.Person.Email,
                Foto = faker.Person.Avatar
            };

            _votoDto = new VotoDto
            {
                IdUsuarioVotado = new Guid()
            };
            _listParametrosVotingHour = new List<Parametro>();
            _listParametrosVotingHour.Add(
                new Parametro(consts.InitialVotingKeyName, "0000")
            );

            _listParametrosVotingHour.Add(
                new Parametro(consts.EndVotingKeyName, "2359")
            );

            _listChavesParametrosFilter = new List<string>();
            _listChavesParametrosFilter.Add(consts.InitialVotingKeyName);
            _listChavesParametrosFilter.Add(consts.EndVotingKeyName);

            _pretendenteRepositoryMock = new Mock<IPretendenteRepository>();
            _parametroRepositoryMock = new Mock<IParametroRepository>();
            _pretendenteService = new PretendenteService(_pretendenteRepositoryMock.Object, _parametroRepositoryMock.Object);
        }

        [Fact]
        public void MustAddVoting()
        {

            var savedPretendente = PretendenteBuilder.New().WithId(_votoDto.IdUsuarioVotado).Build();

            _pretendenteRepositoryMock.Setup(
                p => p.GetById(_votoDto.IdUsuarioVotado)).Returns(savedPretendente);

            _parametroRepositoryMock.Setup(
                p => p.ListByChaves(_listChavesParametrosFilter)).Returns(_listParametrosVotingHour);

            _pretendenteService.AddVoto(_votoDto);

            _pretendenteRepositoryMock.Verify(r => r.Update(
                It.Is<Pretendente>(p=>
                    p.Email.Equals(savedPretendente.Email) &&
                    p.Votos.Count == savedPretendente.Votos.Count
                )
            ));
        }

        [Fact]
        public void MustAddPretendente()
        {

            _pretendenteService.Add(_pretendenteDto);

            _pretendenteRepositoryMock.Verify(r => r.Add(
                It.Is<Pretendente>(
                    p => p.Nome.Equals(_pretendenteDto.Nome) &&
                    p.Email.Equals(_pretendenteDto.Email) &&
                    p.Foto.Equals(_pretendenteDto.Foto)
                )
            ));
        }

        [Fact]
        public void NotShouldAddPretendenteWithEmailDuplicated()
        {
            _response = new HttpServiceResponse<Pretendente>
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = Recourses.ExistingEmail 
            };
            var id = new Guid();
            var savedPretendente = PretendenteBuilder.New().WithId(id).WithEmail(_pretendenteDto.Email).Build();

            _pretendenteRepositoryMock.Setup(
                p => p.GetByEmail(_pretendenteDto.Email)).Returns(savedPretendente);

            _parametroRepositoryMock.Setup(
                p=> p.ListByChaves(_listChavesParametrosFilter)).Returns(_listParametrosVotingHour);

            var expectedResponse = _pretendenteService.Add(_pretendenteDto);
            Assert.Equal(_response.StatusCode, expectedResponse.StatusCode);
            Assert.Equal(_response.Message, expectedResponse.Message);
        }

        [Fact]
        public void NotShouldAddVotoWithInvalidVotingHour()
        {
            _response = new HttpServiceResponse<Pretendente>
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = Recourses.InvalidVotingHour
            };

            var listInvalidParametros = new List<Parametro>();
            listInvalidParametros.Add(
                new Parametro(consts.InitialVotingKeyName, "0000")
            );

            listInvalidParametros.Add(
                new Parametro(consts.EndVotingKeyName, "0001")
            );

            var savedPretendente = PretendenteBuilder.New().Build();

            _pretendenteRepositoryMock.Setup(
                p => p.GetById(_votoDto.IdUsuarioVotado)).Returns(savedPretendente);

            _parametroRepositoryMock.Setup(
                p => p.ListByChaves(_listChavesParametrosFilter)).Returns(listInvalidParametros);

            var expectedResponse = _pretendenteService.AddVoto(_votoDto);
            Assert.Equal(_response.StatusCode, expectedResponse.StatusCode);
            Assert.Equal(_response.Message, expectedResponse.Message);
        }


        [Fact]
        public void NotShouldAddVotoWithInvalidUser()
        {
            _response = new HttpServiceResponse<Pretendente>
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = Recourses.NoExistentPretendente
            };

            Pretendente savedPretendente =null;

            _pretendenteRepositoryMock.Setup(
                p => p.GetById(_votoDto.IdUsuarioVotado)).Returns(savedPretendente);


            _parametroRepositoryMock.Setup(
                p => p.ListByChaves(_listChavesParametrosFilter)).Returns(_listParametrosVotingHour);

            var expectedResponse = _pretendenteService.AddVoto(_votoDto);
            Assert.Equal(_response.StatusCode, expectedResponse.StatusCode);
            Assert.Equal(_response.Message, expectedResponse.Message);
        }

        


    }
}
