﻿using ReiAlmoco.Domain._base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ReiAlmoco.Domain.Models
{
    public class Voto : GenericModel
    {

        #region Atributos
        public DateTime Data { get; private set; }
        public Guid IdUsuarioVotado { get; private set; }

        [ForeignKey("IdUsuarioVotado")]
        public virtual Pretendente Pretendente { get; set; }
        #endregion


        #region Construtores

        private Voto() { }

        public Voto(Guid idUsuarioVotado)
        {
            RulesValidator.New()
                .If(idUsuarioVotado.Equals(null), Recourses.NullUsuarioVotado)
                .DispatchExceptionIfExists();

            Id = new Guid();
            Data = new DateTime();
            IdUsuarioVotado = idUsuarioVotado;
        }
        #endregion


        

    }
}
