﻿using ReiAlmoco.Domain._base;
using System;

namespace ReiAlmoco.Domain.Dtos
{
    public class PretendenteDto
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Foto { get; set; }
    }

}
