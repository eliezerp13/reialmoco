﻿using ReiAlmoco.Domain._base;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReiAlmoco.Domain.Dtos
{
    public class VotoDto
    {
        public Guid Id { get; set; }
        public DateTime Data { get; private set; }
        public Guid IdUsuarioVotado { get; set; }



    }
}
