﻿using ReiAlmoco.Domain.Models;
using System;
using Xunit;

namespace ReiAlmoco.Test.DomainTest
{
    public class VotoTest
    {
        [Fact]
        public void MustCreateVoto()
        {
            var expectedVoto = new
            {
                IdUsuarioVotado = new Guid(),
            };         

            var voto = new Voto(expectedVoto.IdUsuarioVotado);
            Assert.Equal(expectedVoto.IdUsuarioVotado, voto.IdUsuarioVotado);

        }


    }
}
