﻿using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain.Models;
using System;
using System.Collections.Generic;

namespace ReiAlmoco.Domain._utils
{
    public static class Functions
    {
        public static bool isVotingHour(int initialHour, int endHour)
        {

            RulesValidator.New()
                .If(initialHour >= endHour, Recourses.InvalidVotingHourRange)
                .DispatchExceptionIfExists();

            int currentTime = int.Parse(DateTime.Now.ToString("HHmm"));



            if (currentTime >= initialHour && currentTime <= endHour)
                return true;

            else return false;
        }

        public static DateTime GetFirstDateOfWeek(DateTime date)
        {
            if ((int)date.DayOfWeek == (int)DayOfWeek.Sunday)
                return date;

            var newDate = date.AddDays(-(int)date.DayOfWeek);
            return newDate;

        }

        public static DateTime GetLastDateOfWeek(DateTime date)
        {
            if ((int)date.DayOfWeek == (int)DayOfWeek.Saturday)
                return date;

            var newDate = date.AddDays((int)DayOfWeek.Saturday - (int)date.DayOfWeek);
            return newDate;

        }
    }
}
