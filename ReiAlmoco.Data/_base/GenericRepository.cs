﻿using Microsoft.EntityFrameworkCore;
using ReiAlmoco.Domain._base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReiAlmoco.Data._base
{
    public class GenericRepository<T> : IGenericRepository<T> where T : GenericModel
    {
        protected readonly AppDbContext _context;


        public GenericRepository(AppDbContext context)
        {
            _context = context;
            
        }

        public virtual void Add(T entity)
        {
            try
            {
                 _context.Set<T>().Add(entity);
                _context.SaveChanges();

            }catch(Exception ex)
            {
                throw ex;
            }
            
        }

        public bool Delete(Guid id)
        {
            try
            {
                var result = _context.Set<T>().SingleOrDefaultAsync(e => e.Id == e.Id);
                _context.Remove(result);
                 _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<T> List()
        {
            try
            {
                return _context.Set<T>().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(T entity)
        {
            try
            {
                var result = _context.Set<T>().SingleOrDefault(e => e.Id == e.Id);
                result = entity;
                _context.Entry(result).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual T GetById(Guid id)
        {
            try
            {
                return _context.Set<T>().SingleOrDefault(e => e.Id == id);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
