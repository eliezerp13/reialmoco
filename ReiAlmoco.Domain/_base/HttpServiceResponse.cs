﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ReiAlmoco.Domain._base
{
    public class HttpServiceResponse<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public T Object { get; set; }
        public string Message{get; set;}
        public IEnumerable<T> ListObject {get; set;}

    }
}
