﻿using Bogus;
using ReiAlmoco.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReiAlmoco.Test._builders
{
    public class PretendenteBuilder
    {
        protected Guid Id;
        protected string Nome;
        protected string Email;
        protected string Foto;

        public static PretendenteBuilder New()
        {
            var faker = new Faker();
            return new PretendenteBuilder
            {
                Nome = faker.Person.FullName,
                Email = faker.Person.Email,
                Foto = faker.Person.Avatar
            };
        }

        public PretendenteBuilder WithNome(string nome)
        {
            Nome = nome;
            return this;
        }

        public PretendenteBuilder WithEmail(string email)
        {
            Email = email;
            return this;
        }

        public PretendenteBuilder WithFoto(string foto)
        {
            Foto = foto;
            return this;
        }

        public PretendenteBuilder WithId(Guid id)
        {
            Id = id;
            return this;
        }

        public Pretendente Build()
        {
            return new Pretendente(Nome, Email, Foto);

        }
    }
}
