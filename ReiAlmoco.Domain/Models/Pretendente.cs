﻿using ReiAlmoco.Domain._base;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;

namespace ReiAlmoco.Domain.Models
{
    public class Pretendente : GenericModel
    {
        private readonly Regex _emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

        #region Atributos
        private Pretendente() { }
        public string Nome { get; private set; }

        public string Email { get; private set; }
        public string Foto { get; private set; }

        public virtual ICollection<Voto> Votos { get; set; }

        #endregion


        #region Construtores
        public Pretendente(string nome, string email, string foto)
        {
            

            RulesValidator.New()
                .If(string.IsNullOrEmpty(nome), Recourses.NullOrEmptyNome)
                .If(nome != null && nome.Length > 100, Recourses.OutOfRangeNome)
                .If(string.IsNullOrEmpty(email), Recourses.NullOrEmptyEmail)
                .If(email != null && email.Length > 150, Recourses.OutOfRangeEmail)
                .If(email != null && !_emailRegex.Match(email).Success, Recourses.InvalidEmail)
                .If(foto != null && !string.IsNullOrEmpty(foto) && foto.Length > 255, Recourses.OutOfRangeFoto)
                .DispatchExceptionIfExists();

            Id = new Guid();
            Nome = nome;
            Email = email;
            Foto = foto;
            Votos = new List<Voto>();
        }

        #endregion



    }

}
