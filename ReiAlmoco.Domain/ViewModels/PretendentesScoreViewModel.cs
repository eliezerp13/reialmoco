﻿using ReiAlmoco.Domain.Models;
using System;
using System.Collections.Generic;

namespace ReiAlmoco.Domain.ViewModels
{
    public class PretendentesScoreViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }

        public string Email { get; set; }
        public string Foto { get; set; }

        public int TotalVotos { get; set; }
    }
}
