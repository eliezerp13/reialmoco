﻿using ReiAlmoco.Domain._base;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReiAlmoco.Domain.Models
{
    public class Parametro : GenericModel
    {
        public string Chave { get; set; }
        public string Valor { get; set; }
        private Parametro()
        {

        }
        public Parametro(string chave, string valor)
        {
            Id = new Guid();
            Chave = chave;
            Valor = valor;
        }

        

        
       
    }
}
