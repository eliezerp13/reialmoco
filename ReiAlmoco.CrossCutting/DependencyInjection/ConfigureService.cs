﻿using Microsoft.Extensions.DependencyInjection;
using ReiAlmoco.Domain.Interfaces.Services;
using ReiAlmoco.Service.Services;

namespace ReiAlmoco.CrossCutting.DependencyInjection
{
    public class ConfigureService
    {
        public static void ConfigureDependenciesService(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IPretendenteService, PretendenteService>();
            serviceCollection.AddTransient<IEmailService, EmailService>();
        }
    }
}
