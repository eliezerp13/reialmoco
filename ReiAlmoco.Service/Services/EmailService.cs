﻿using ReiAlmoco.Domain;
using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain.Interfaces.Repositories;
using ReiAlmoco.Domain.Interfaces.Services;
using ReiAlmoco.Domain.Models;
using ReiAlmoco.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;

namespace ReiAlmoco.Service.Services
{
    public class EmailService : IEmailService
    {
        public readonly IPretendenteRepository _pretendenteRepository;
        public readonly IParametroRepository _parametroRepository;

        public EmailService(IParametroRepository parametroRepository, IPretendenteRepository pretendenteRepository)
        {
            _parametroRepository = parametroRepository;
            _pretendenteRepository = pretendenteRepository;
        }

        public HttpServiceResponse<PretendentesScoreViewModel> DispatchEmailToKingOfTheDay()
        {
            var king = _pretendenteRepository.GetByMostVotedByDay(DateTime.Now);
            if (king == null)
                return new HttpServiceResponse<PretendentesScoreViewModel>()
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Message = Recourses.NotVotings
                };

            var listParams = LoadParamsToListEmailConfig();
            var parametros = _parametroRepository.ListByChaves(listParams);

            var emailJob = new EmailJob(
                parametros.Where(p => p.Chave.Equals(consts.SMTPKeyName)).FirstOrDefault().Valor,
                parametros.Where(p => p.Chave.Equals(consts.LoginEmailKeyName)).FirstOrDefault().Valor,
                parametros.Where(p => p.Chave.Equals(consts.PasswordEmailKeyName)).FirstOrDefault().Valor,
                parametros.Where(p => p.Chave.Equals(consts.PortEmailKeyName)).FirstOrDefault().Valor,
                parametros.Where(p => p.Chave.Equals(consts.MessageKeyName)).FirstOrDefault().Valor,
                parametros.Where(p => p.Chave.Equals(consts.EmailSubjectKeyName)).FirstOrDefault().Valor,
                parametros.Where(p => p.Chave.Equals(consts.SSLKeyName)).FirstOrDefault().Valor
                );

            var messagemail = new MailMessage();
            messagemail.From = new MailAddress(emailJob.Login);
            messagemail.To.Add(king.Email); 
            messagemail.Subject = emailJob.Subject ; 
            messagemail.Body = emailJob.Message;

            using (var smtp = new SmtpClient(emailJob.SMTPClient, emailJob.Port))
            {
                smtp.EnableSsl = emailJob.EmailSSL;
                smtp.Credentials = new NetworkCredential(emailJob.Login, emailJob.Password);
                smtp.Send(messagemail);

            }

            return new HttpServiceResponse<PretendentesScoreViewModel>
            {
                StatusCode = HttpStatusCode.OK,
                Message = Recourses.SuccessSendEmail
            };

        }

        private List<string> LoadParamsToListEmailConfig()
        {
            var list = new List<string>();
            list.Add(consts.SMTPKeyName);
            list.Add(consts.LoginEmailKeyName);
            list.Add(consts.PasswordEmailKeyName);
            list.Add(consts.MessageKeyName);
            list.Add(consts.PortEmailKeyName);
            list.Add(consts.EmailSubjectKeyName);
            list.Add(consts.SSLKeyName);

            return list;

        }
    }
}
