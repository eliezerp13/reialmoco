﻿using ReiAlmoco.Data._base;
using ReiAlmoco.Domain.Interfaces.Repositories;
using ReiAlmoco.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReiAlmoco.Data.Repositories
{
    public class ParametroRepository : GenericRepository<Parametro>, IParametroRepository
    {
        private readonly AppDbContext _context;

        public ParametroRepository(AppDbContext context) : base(context)
        {
            _context = context;

        }
        public List<Parametro> ListByChaves(List<string> chaves)
        {
            var list = new List<Parametro>();
            
            var query =  _context.Set<Parametro>().AsQueryable();

            foreach(string chave in chaves)
            {
                var parametro = query.Where(p => p.Chave.Equals(chave)).FirstOrDefault();
                list.Add(parametro);
            }
            return list;

        }
    }
}
