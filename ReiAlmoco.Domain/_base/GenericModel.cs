﻿using System;

namespace ReiAlmoco.Domain._base
{
    public abstract class GenericModel
    {
        public Guid Id { get; protected set; }
    }
}
