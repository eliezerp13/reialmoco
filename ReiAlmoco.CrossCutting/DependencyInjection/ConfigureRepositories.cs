﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ReiAlmoco.Data;
using ReiAlmoco.Data._base;
using ReiAlmoco.Data.Repositories;
using ReiAlmoco.Domain._base;
using ReiAlmoco.Domain.Interfaces.Repositories;
using System.Configuration;

namespace ReiAlmoco.CrossCutting.DependencyInjection
{
    public class ConfigureRepositories
    {
        public static void ConfigureDependenciesRepository(IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            serviceCollection.AddScoped<IPretendenteRepository, PretendenteRepository>();
            serviceCollection.AddScoped<IParametroRepository, ParametroRepository>();


            serviceCollection.AddDbContext<AppDbContext>(
                options => options.UseSqlServer(
                    configuration["ConnectionString"].ToString())
            ); ;

            serviceCollection.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            serviceCollection.AddScoped<IPretendenteRepository, PretendenteRepository>();
            serviceCollection.AddScoped<IParametroRepository, ParametroRepository>();


          
        }
    }
}
