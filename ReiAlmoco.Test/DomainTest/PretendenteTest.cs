﻿using Bogus;
using ExpectedObjects;
using ReiAlmoco.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using ReiAlmoco.Test._builders;
using ReiAlmoco.Domain._base;
using ReiAlmoco.Test._utils;

namespace ReiAlmoco.Test.DomainTest
{
    public class PretendenteTest
    {
        private readonly Faker _faker;

        public PretendenteTest()
        {
            _faker = new Faker();
        }

        [Fact]
        public void MustCreatePretendente()
        {
            var expectedPretendente = new
            {
                Nome = _faker.Person.FullName,
                Email = _faker.Person.Email,
                Foto = _faker.Person.Avatar
            };

            var pretendente = new Pretendente(expectedPretendente.Nome, expectedPretendente.Email, expectedPretendente.Foto);

            expectedPretendente.ToExpectedObject().ShouldMatch(pretendente);
        }

        

        #region EmailTest

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void NotShouldCreateWithNullOrEmptyEmail(string email)
        {
            Assert.Throws<DomainException>(() =>
                PretendenteBuilder.New().WithEmail(email).Build()
            ).WithMessage(Recourses.NullOrEmptyEmail);
        }

        [Fact]
        public void NotShouldCreateOutOfRangeEmail()
        {

            Assert.Throws<DomainException>(() =>
                PretendenteBuilder.New().WithEmail(_faker.Random.String(151)).Build()
            ).WithMessage(Recourses.OutOfRangeEmail);
        }

        [Theory]
        [InlineData("invalid email")]
        public void NotShouldCreateInvalidStringEmail(string email)
        {
            Assert.Throws<DomainException>(() =>
                PretendenteBuilder.New().WithEmail(email).Build()
            ).WithMessage(Recourses.InvalidEmail);
        }

        #endregion

        #region NomeTest

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void NotShouldCreateWithNullOrEmptyNome(string nome)
        {
            Assert.Throws<DomainException>(() =>
                PretendenteBuilder.New().WithNome(nome).Build()
            ).WithMessage(Recourses.NullOrEmptyNome);
        }

        [Fact]
        public void NotShouldCreateOutOfRangeNome()
        {

            Assert.Throws<DomainException>(() =>
                PretendenteBuilder.New().WithNome(_faker.Random.String(151)).Build()
            ).WithMessage(Recourses.OutOfRangeNome);
        }
        #endregion


        #region FotoTest

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void MustCreatePretendenteWithNullOrEmptyFoto(string foto)
        {
            var expectedPretendente = new
            {
                Nome = _faker.Person.FullName,
                Email = _faker.Person.Email,
            };

            var pretendente = new Pretendente(expectedPretendente.Nome, expectedPretendente.Email, foto);

            expectedPretendente.ToExpectedObject().ShouldMatch(pretendente);
        }

        [Fact]
        public void NotShouldCreateOutOfRangeFoto()
        {

            Assert.Throws<DomainException>(() =>
                PretendenteBuilder.New().WithNome(_faker.Random.String(256)).Build()
            ).WithMessage(Recourses.OutOfRangeNome);
        }
        #endregion

    }
}
