﻿using ReiAlmoco.Domain._base;

namespace ReiAlmoco.Domain.Models
{
    public class EmailJob
    {
        public EmailJob(string sMTPClient, string login, string password, string port, string message, string subject, string emailSSL)
        {
            RulesValidator.New()
                .If(!string.IsNullOrEmpty(SMTPClient), Recourses.NullOrEmptyKey(consts.SMTPKeyName))
                .If(!string.IsNullOrEmpty(login), Recourses.NullOrEmptyKey(consts.LoginEmailKeyName))
                .If(!string.IsNullOrEmpty(password), Recourses.NullOrEmptyKey(consts.PasswordEmailKeyName))
                .If(!string.IsNullOrEmpty(port) || !int.TryParse(port, out int portInt), Recourses.NullOrEmptyKey(consts.PortEmailKeyName))
                .If(!string.IsNullOrEmpty(message), Recourses.NullOrEmptyKey(consts.MessageKeyName))
                .If(!string.IsNullOrEmpty(subject), Recourses.NullOrEmptyKey(consts.EmailSubjectKeyName))
                .If(!string.IsNullOrEmpty(emailSSL) || !bool.TryParse(emailSSL, out bool ssl), Recourses.NullOrEmptyKey(consts.SSLKeyName));;
                
            SMTPClient = sMTPClient;
            Login = login;
            Password = password;
            Port = int.Parse(port);
            Message = message;
            Subject = subject;
            EmailSSL = bool.Parse(emailSSL);
        }

        public string SMTPClient { get; private set; }
        public string Login { get; private set; }
        public string Password { get; private set; }
        public int Port { get; private set; }
        public string Message { get; private set; }
        public string Subject { get; private set; }
        public bool EmailSSL { get; private set; }
    }
}
