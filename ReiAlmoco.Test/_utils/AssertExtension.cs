﻿using ReiAlmoco.Domain._base;
using Xunit;

namespace ReiAlmoco.Test._utils
{
    public static class AssertExtension
    {
        public static void WithMessage(this DomainException exception, string message)
        {
            if (exception.ErrorMessages.Contains(message))
                Assert.True(true);
            else
                Assert.False(true, $"Esperava a mensagem '{message}'");
        }
    }
}
