﻿namespace ReiAlmoco.Domain._base
{
    public static class Recourses
    {
        #region Errors
        public static string InvalidEmail = "Endereço de e-mail inválido";
        public static string ExistingEmail = "E-mail já cadastrado no sistema";
        public static string NotVotings = "Nenhum voto encontrado no sistema";
        public static string NullOrEmptyEmail = "O campo e-mail não pode ser branco ou nulo";
        public static string NullOrEmptyNome = "O campo nome não pode ser branco ou nulo";
        public static string OutOfRangeEmail = "O campo e-mail não deve conter mais que 100 caracteres";
        public static string OutOfRangeNome = "O campo nome não deve conter mais que 100 caracteres";
        public static string OutOfRangeFoto = "O campo foto não deve conter mais que 255 caracteres";
        public static string NoExistentPretendente = "Pretendente não registrado no banco";
        public static string NoExistentPretendenteByFilter = "Nenhum pretendente foi encontrado";
        public static string NullUsuarioVotado = "Um usuário precisa ser selecionado";
        public static string InvalidVotingHourRange = "Range de horário de votação inválido";
        public static string InvalidVotingHour = "Fora do horário de votação";
        public static string InvalidVotingHourParameter = "Parâmetro de hora inválido";
        

        public static string NullOrEmptyKey(string chave)
        {
            return $"A chave {chave} está incorreta.  Verifique a tabela {consts.TabelaParametro}";
        }
        #endregion

        #region Success
        public static string PretendenteSuccess = "Pretendente registrado com sucesso";
        public static string VotoSuccess = "Voto registrado com sucesso";
        public static string SuccessSendEmail = "O e-mail foi enviado";
        #endregion

        public static string UnespectedException = "Ocorreu um erro inesperado! Tente novamente mais tarde";

    }
}
