﻿namespace ReiAlmoco.Domain._utils
{
    public enum PretendenteFilterType
    {
        MoreVotedByDay,
        MoreVotedByWeek,
        FewerVotedByWeek

    }

    public enum DayOfWeek
    {
        Sunday,
        Monday,
        Thuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday
    }
}
