﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReiAlmoco.Domain._base
{
    public class RulesValidator
    {
        private readonly List<string> _errorMessages;

        private RulesValidator()
        {
            _errorMessages = new List<string>();
        }

        public static RulesValidator New()
        {
            return new RulesValidator();
        }

        public RulesValidator If(bool hasError, string errorMessage)
        {
            if (hasError)
                _errorMessages.Add(errorMessage);

            return this;
        }

        public void DispatchExceptionIfExists()
        {
            if (_errorMessages.Any())
                throw new DomainException(_errorMessages);
        }
    }

    public class DomainException : ArgumentException
    {
        public List<string> ErrorMessages { get; set; }

        public DomainException(List<string> errorMessages)
        {
            ErrorMessages = errorMessages;
        }
    }
}

