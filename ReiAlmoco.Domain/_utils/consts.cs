﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReiAlmoco.Domain
{
    public static class consts
    {
        public const string TabelaParametro = "PARAMETROS";

        public const string InitialVotingKeyName = "InitialVotingHour";
        public const string EndVotingKeyName = "EndVotingHour";

        public const string SMTPKeyName = "EmailSMTPClient";
        public const string LoginEmailKeyName = "EmailLoginCred";
        public const string PasswordEmailKeyName = "EmailPasswordCred";
        public const string MessageKeyName = "EmailMessage";
        public const string PortEmailKeyName = "EmailPort";
        public const string EmailSubjectKeyName = "EmailSubject";
        public const string SSLKeyName = "EmailSSL";
    }
}
